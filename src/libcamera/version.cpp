/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019, Google Inc.
 *
 * version.cpp - libcamera version
 *
 * This file is auto-generated. Do not edit.
 */

#include <libcamera/camera_manager.h>

namespace libcamera {

const std::string CameraManager::version_("v0.0.0+2454-ac0c68ea");

} /* namespace libcamera */
