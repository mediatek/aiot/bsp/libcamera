/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019, Google Inc.
 *
 * control_ids.cpp : Control ID list
 *
 * This file is auto-generated. Do not edit.
 */

#include <libcamera/control_ids.h>
#include <libcamera/controls.h>

/**
 * \file control_ids.h
 * \brief Camera control identifiers
 */

namespace libcamera {

/**
 * \brief Namespace for libcamera controls
 */
namespace controls {

/**
 * \var AeEnable
 * \brief Enable or disable the AE.
 *
 * \sa ExposureTime AnalogueGain
 */

/**
 * \var AeLocked
 * \brief Report the lock status of a running AE algorithm.
 *
 * If the AE algorithm is locked the value shall be set to true, if it's
 * converging it shall be set to false. If the AE algorithm is not
 * running the control shall not be present in the metadata control list.
 *
 * \sa AeEnable
 */

/**
 * \enum AeMeteringModeEnum
 * \brief Supported AeMeteringMode values
 *
 * \var MeteringCentreWeighted
 * \brief Centre-weighted metering mode.
 *
 * \var MeteringSpot
 * \brief Spot metering mode.
 *
 * \var MeteringMatrix
 * \brief Matrix metering mode.
 *
 * \var MeteringCustom
 * \brief Custom metering mode.
 */

/**
 * \var AeMeteringModeValues
 * \brief List of all AeMeteringMode supported values
 */

/**
 * \var AeMeteringMode
 * \brief Specify a metering mode for the AE algorithm to use. The metering
 * modes determine which parts of the image are used to determine the
 * scene brightness. Metering modes may be platform specific and not
 * all metering modes may be supported.
 */

/**
 * \enum AeConstraintModeEnum
 * \brief Supported AeConstraintMode values
 *
 * \var ConstraintNormal
 * \brief Default constraint mode. This mode aims to balance the exposure of different parts of the image so as to reach a reasonable average level. However, highlights in the image may appear over-exposed and lowlights may appear under-exposed.
 *
 * \var ConstraintHighlight
 * \brief Highlight constraint mode. This mode adjusts the exposure levels in order to try and avoid over-exposing the brightest parts (highlights) of an image. Other non-highlight parts of the image may appear under-exposed.
 *
 * \var ConstraintShadows
 * \brief Shadows constraint mode. This mode adjusts the exposure levels in order to try and avoid under-exposing the dark parts (shadows) of an image. Other normally exposed parts of the image may appear over-exposed.
 *
 * \var ConstraintCustom
 * \brief Custom constraint mode.
 */

/**
 * \var AeConstraintModeValues
 * \brief List of all AeConstraintMode supported values
 */

/**
 * \var AeConstraintMode
 * \brief Specify a constraint mode for the AE algorithm to use. These determine
 * how the measured scene brightness is adjusted to reach the desired
 * target exposure. Constraint modes may be platform specific, and not
 * all constraint modes may be supported.
 */

/**
 * \enum AeExposureModeEnum
 * \brief Supported AeExposureMode values
 *
 * \var ExposureNormal
 * \brief Default exposure mode.
 *
 * \var ExposureShort
 * \brief Exposure mode allowing only short exposure times.
 *
 * \var ExposureLong
 * \brief Exposure mode allowing long exposure times.
 *
 * \var ExposureCustom
 * \brief Custom exposure mode.
 */

/**
 * \var AeExposureModeValues
 * \brief List of all AeExposureMode supported values
 */

/**
 * \var AeExposureMode
 * \brief Specify an exposure mode for the AE algorithm to use. These specify
 * how the desired total exposure is divided between the shutter time
 * and the sensor's analogue gain. The exposure modes are platform
 * specific, and not all exposure modes may be supported.
 */

/**
 * \var ExposureValue
 * \brief Specify an Exposure Value (EV) parameter. The EV parameter will only be
 * applied if the AE algorithm is currently enabled.
 *
 * By convention EV adjusts the exposure as log2. For example
 * EV = [-2, -1, 0.5, 0, 0.5, 1, 2] results in an exposure adjustment
 * of [1/4x, 1/2x, 1/sqrt(2)x, 1x, sqrt(2)x, 2x, 4x].
 *
 * \sa AeEnable
 */

/**
 * \var ExposureTime
 * \brief Exposure time (shutter speed) for the frame applied in the sensor
 * device. This value is specified in micro-seconds.
 *
 * Setting this value means that it is now fixed and the AE algorithm may
 * not change it. Setting it back to zero returns it to the control of the
 * AE algorithm.
 *
 * \sa AnalogueGain AeEnable
 *
 * \todo Document the interactions between AeEnable and setting a fixed
 * value for this control. Consider interactions with other AE features,
 * such as aperture and aperture/shutter priority mode, and decide if
 * control of which features should be automatically adjusted shouldn't
 * better be handled through a separate AE mode control.
 */

/**
 * \var AnalogueGain
 * \brief Analogue gain value applied in the sensor device.
 * The value of the control specifies the gain multiplier applied to all
 * colour channels. This value cannot be lower than 1.0.
 *
 * Setting this value means that it is now fixed and the AE algorithm may
 * not change it. Setting it back to zero returns it to the control of the
 * AE algorithm.
 *
 * \sa ExposureTime AeEnable
 *
 * \todo Document the interactions between AeEnable and setting a fixed
 * value for this control. Consider interactions with other AE features,
 * such as aperture and aperture/shutter priority mode, and decide if
 * control of which features should be automatically adjusted shouldn't
 * better be handled through a separate AE mode control.
 */

/**
 * \var Brightness
 * \brief Specify a fixed brightness parameter. Positive values (up to 1.0)
 * produce brighter images; negative values (up to -1.0) produce darker
 * images and 0.0 leaves pixels unchanged.
 */

/**
 * \var Contrast
 * \brief Specify a fixed contrast parameter. Normal contrast is given by the
 * value 1.0; larger values produce images with more contrast.
 */

/**
 * \var Lux
 * \brief Report an estimate of the current illuminance level in lux. The Lux
 * control can only be returned in metadata.
 */

/**
 * \var AwbEnable
 * \brief Enable or disable the AWB.
 *
 * \sa ColourGains
 */

/**
 * \enum AwbModeEnum
 * \brief Supported AwbMode values
 *
 * \var AwbAuto
 * \brief Search over the whole colour temperature range.
 *
 * \var AwbIncandescent
 * \brief Incandescent AWB lamp mode.
 *
 * \var AwbTungsten
 * \brief Tungsten AWB lamp mode.
 *
 * \var AwbFluorescent
 * \brief Fluorescent AWB lamp mode.
 *
 * \var AwbIndoor
 * \brief Indoor AWB lighting mode.
 *
 * \var AwbDaylight
 * \brief Daylight AWB lighting mode.
 *
 * \var AwbCloudy
 * \brief Cloudy AWB lighting mode.
 *
 * \var AwbCustom
 * \brief Custom AWB mode.
 */

/**
 * \var AwbModeValues
 * \brief List of all AwbMode supported values
 */

/**
 * \var AwbMode
 * \brief Specify the range of illuminants to use for the AWB algorithm. The modes
 * supported are platform specific, and not all modes may be supported.
 */

/**
 * \var AwbLocked
 * \brief Report the lock status of a running AWB algorithm.
 *
 * If the AWB algorithm is locked the value shall be set to true, if it's
 * converging it shall be set to false. If the AWB algorithm is not
 * running the control shall not be present in the metadata control list.
 *
 * \sa AwbEnable
 */

/**
 * \var ColourGains
 * \brief Pair of gain values for the Red and Blue colour channels, in that
 * order. ColourGains can only be applied in a Request when the AWB is
 * disabled.
 *
 * \sa AwbEnable
 */

/**
 * \var ColourTemperature
 * \brief Report the current estimate of the colour temperature, in kelvin, for this frame. The ColourTemperature control can only be returned in metadata.
 */

/**
 * \var Saturation
 * \brief Specify a fixed saturation parameter. Normal saturation is given by
 * the value 1.0; larger values produce more saturated colours; 0.0
 * produces a greyscale image.
 */

/**
 * \var SensorBlackLevels
 * \brief Reports the sensor black levels used for processing a frame, in the
 * order R, Gr, Gb, B. These values are returned as numbers out of a 16-bit
 * pixel range (as if pixels ranged from 0 to 65535). The SensorBlackLevels
 * control can only be returned in metadata.
 */

/**
 * \var Sharpness
 * \brief A value of 0.0 means no sharpening. The minimum value means
 * minimal sharpening, and shall be 0.0 unless the camera can't
 * disable sharpening completely. The default value shall give a
 * "reasonable" level of sharpening, suitable for most use cases.
 * The maximum value may apply extremely high levels of sharpening,
 * higher than anyone could reasonably want. Negative values are
 * not allowed. Note also that sharpening is not applied to raw
 * streams.
 */

/**
 * \var FocusFoM
 * \brief Reports a Figure of Merit (FoM) to indicate how in-focus the frame is.
 * A larger FocusFoM value indicates a more in-focus frame. This control
 * depends on the IPA to gather ISP statistics from the defined focus
 * region, and combine them in a suitable way to generate a FocusFoM value.
 * In this respect, it is not necessarily aimed at providing a way to
 * implement a focus algorithm by the application, rather an indication of
 * how in-focus a frame is.
 */

/**
 * \var ColourCorrectionMatrix
 * \brief The 3x3 matrix that converts camera RGB to sRGB within the
 * imaging pipeline. This should describe the matrix that is used
 * after pixels have been white-balanced, but before any gamma
 * transformation. The 3x3 matrix is stored in conventional reading
 * order in an array of 9 floating point values.
 */

/**
 * \var ScalerCrop
 * \brief Sets the image portion that will be scaled to form the whole of
 * the final output image. The (x,y) location of this rectangle is
 * relative to the PixelArrayActiveAreas that is being used. The units
 * remain native sensor pixels, even if the sensor is being used in
 * a binning or skipping mode.
 *
 * This control is only present when the pipeline supports scaling. Its
 * maximum valid value is given by the properties::ScalerCropMaximum
 * property, and the two can be used to implement digital zoom.
 */

/**
 * \var DigitalGain
 * \brief Digital gain value applied during the processing steps applied
 * to the image as captured from the sensor.
 *
 * The global digital gain factor is applied to all the colour channels
 * of the RAW image. Different pipeline models are free to
 * specify how the global gain factor applies to each separate
 * channel.
 *
 * If an imaging pipeline applies digital gain in distinct
 * processing steps, this value indicates their total sum.
 * Pipelines are free to decide how to adjust each processing
 * step to respect the received gain factor and shall report
 * their total value in the request metadata.
 */

/**
 * \var FrameDurations
 * \brief The minimum and maximum (in that order) frame duration,
 * expressed in microseconds.
 *
 * When provided by applications, the control specifies the sensor frame
 * duration interval the pipeline has to use. This limits the largest
 * exposure time the sensor can use. For example, if a maximum frame
 * duration of 33ms is requested (corresponding to 30 frames per second),
 * the sensor will not be able to raise the exposure time above 33ms.
 * A fixed frame duration is achieved by setting the minimum and maximum
 * values to be the same. Setting both values to 0 reverts to using the
 * IPA provided defaults.
 *
 * The maximum frame duration provides the absolute limit to the shutter
 * speed computed by the AE algorithm and it overrides any exposure mode
 * setting specified with controls::AeExposureMode. Similarly, when a
 * manual exposure time is set through controls::ExposureTime, it also
 * gets clipped to the limits set by this control. When reported in
 * metadata, the control expresses the minimum and maximum frame
 * durations used after being clipped to the sensor provided frame
 * duration limits.
 *
 * \sa AeExposureMode
 * \sa ExposureTime
 *
 * \todo Define how to calculate the capture frame rate by
 * defining controls to report additional delays introduced by
 * the capture pipeline or post-processing stages (ie JPEG
 * conversion, frame scaling).
 *
 * \todo Provide an explicit definition of default control values, for
 * this and all other controls.
 */

/**
 * \brief Namespace for libcamera draft controls
 */
namespace draft {

/**
 * \enum AePrecaptureTriggerEnum
 * \brief Supported AePrecaptureTrigger values
 *
 * \var AePrecaptureTriggerIdle
 * \brief The trigger is idle.
 *
 * \var AePrecaptureTriggerStart
 * \brief The pre-capture AE metering is started by the camera.
 *
 * \var AePrecaptureTriggerCancel
 * \brief The camera will cancel any active or completed metering sequence.
 * The AE algorithm is reset to its initial state.
 */

/**
 * \var AePrecaptureTriggerValues
 * \brief List of all AePrecaptureTrigger supported values
 */

/**
 * \var AePrecaptureTrigger
 * \brief Control for AE metering trigger. Currently identical to
 * ANDROID_CONTROL_AE_PRECAPTURE_TRIGGER.
 *
 * Whether the camera device will trigger a precapture metering sequence
 * when it processes this request.
 */

/**
 * \enum AfTriggerEnum
 * \brief Supported AfTrigger values
 *
 * \var AfTriggerIdle
 * \brief The trigger is idle.
 *
 * \var AfTriggerStart
 * \brief The AF routine is started by the camera.
 *
 * \var AfTriggerCancel
 * \brief The camera will cancel any active trigger and the AF routine is
 * reset to its initial state.
 */

/**
 * \var AfTriggerValues
 * \brief List of all AfTrigger supported values
 */

/**
 * \var AfTrigger
 * \brief Control for AF trigger. Currently identical to
 * ANDROID_CONTROL_AF_TRIGGER.
 *
 *  Whether the camera device will trigger autofocus for this request.
 */

/**
 * \enum NoiseReductionModeEnum
 * \brief Supported NoiseReductionMode values
 *
 * \var NoiseReductionModeOff
 * \brief No noise reduction is applied
 *
 * \var NoiseReductionModeFast
 * \brief Noise reduction is applied without reducing the frame rate.
 *
 * \var NoiseReductionModeHighQuality
 * \brief High quality noise reduction at the expense of frame rate.
 *
 * \var NoiseReductionModeMinimal
 * \brief Minimal noise reduction is applied without reducing the frame rate.
 *
 * \var NoiseReductionModeZSL
 * \brief Noise reduction is applied at different levels to different streams.
 */

/**
 * \var NoiseReductionModeValues
 * \brief List of all NoiseReductionMode supported values
 */

/**
 * \var NoiseReductionMode
 * \brief Control to select the noise reduction algorithm mode. Currently
 * identical to ANDROID_NOISE_REDUCTION_MODE.
 *
 *  Mode of operation for the noise reduction algorithm.
 */

/**
 * \enum ColorCorrectionAberrationModeEnum
 * \brief Supported ColorCorrectionAberrationMode values
 *
 * \var ColorCorrectionAberrationOff
 * \brief No aberration correction is applied.
 *
 * \var ColorCorrectionAberrationFast
 * \brief Aberration correction will not slow down the frame rate.
 *
 * \var ColorCorrectionAberrationHighQuality
 * \brief High quality aberration correction which might reduce the frame
 * rate.
 */

/**
 * \var ColorCorrectionAberrationModeValues
 * \brief List of all ColorCorrectionAberrationMode supported values
 */

/**
 * \var ColorCorrectionAberrationMode
 * \brief Control to select the color correction aberration mode. Currently
 * identical to ANDROID_COLOR_CORRECTION_ABERRATION_MODE.
 *
 *  Mode of operation for the chromatic aberration correction algorithm.
 */

/**
 * \enum AeStateEnum
 * \brief Supported AeState values
 *
 * \var AeStateInactive
 * \brief The AE algorithm is inactive.
 *
 * \var AeStateSearching
 * \brief The AE algorithm has not converged yet.
 *
 * \var AeStateConverged
 * \brief The AE algorithm has converged.
 *
 * \var AeStateLocked
 * \brief The AE algorithm is locked.
 *
 * \var AeStateFlashRequired
 * \brief The AE algorithm would need a flash for good results
 *
 * \var AeStatePrecapture
 * \brief The AE algorithm has started a pre-capture metering session.
 * \sa AePrecaptureTrigger
 */

/**
 * \var AeStateValues
 * \brief List of all AeState supported values
 */

/**
 * \var AeState
 * \brief Control to report the current AE algorithm state. Currently identical to
 * ANDROID_CONTROL_AE_STATE.
 *
 *  Current state of the AE algorithm.
 */

/**
 * \enum AfStateEnum
 * \brief Supported AfState values
 *
 * \var AfStateInactive
 * \brief The AF algorithm is inactive.
 *
 * \var AfStatePassiveScan
 * \brief AF is performing a passive scan of the scene in continuous
 * auto-focus mode.
 *
 * \var AfStatePassiveFocused
 * \brief AF believes the scene is in focus, but might restart scanning.
 *
 * \var AfStateActiveScan
 * \brief AF is performing a scan triggered by an AF trigger request.
 * \sa AfTrigger
 *
 * \var AfStateFocusedLock
 * \brief AF believes has focused correctly and has locked focus.
 *
 * \var AfStateNotFocusedLock
 * \brief AF has not been able to focus and has locked.
 *
 * \var AfStatePassiveUnfocused
 * \brief AF has completed a passive scan without finding focus.
 */

/**
 * \var AfStateValues
 * \brief List of all AfState supported values
 */

/**
 * \var AfState
 * \brief Control to report the current AF algorithm state. Currently identical to
 * ANDROID_CONTROL_AF_STATE.
 *
 *  Current state of the AF algorithm.
 */

/**
 * \enum AwbStateEnum
 * \brief Supported AwbState values
 *
 * \var AwbStateInactive
 * \brief The AWB algorithm is inactive.
 *
 * \var AwbStateSearching
 * \brief The AWB algorithm has not converged yet.
 *
 * \var AwbConverged
 * \brief The AWB algorithm has converged.
 *
 * \var AwbLocked
 * \brief The AWB algorithm is locked.
 */

/**
 * \var AwbStateValues
 * \brief List of all AwbState supported values
 */

/**
 * \var AwbState
 * \brief Control to report the current AWB algorithm state. Currently identical
 * to ANDROID_CONTROL_AWB_STATE.
 *
 *  Current state of the AWB algorithm.
 */

/**
 * \var SensorTimestamp
 * \brief Control to report the start of exposure of the first row of the captured
 * image. Currently identical to ANDROID_SENSOR_TIMESTAMP.
 */

/**
 * \var SensorRollingShutterSkew
 * \brief Control to report the time between the start of exposure of the first
 * row and the start of exposure of the last row. Currently identical to
 * ANDROID_SENSOR_ROLLING_SHUTTER_SKEW
 */

/**
 * \enum LensShadingMapModeEnum
 * \brief Supported LensShadingMapMode values
 *
 * \var LensShadingMapModeOff
 * \brief No lens shading map mode is available.
 *
 * \var LensShadingMapModeOn
 * \brief The lens shading map mode is available.
 */

/**
 * \var LensShadingMapModeValues
 * \brief List of all LensShadingMapMode supported values
 */

/**
 * \var LensShadingMapMode
 * \brief Control to report if the lens shading map is available. Currently
 * identical to ANDROID_STATISTICS_LENS_SHADING_MAP_MODE.
 */

/**
 * \enum SceneFlickerEnum
 * \brief Supported SceneFlicker values
 *
 * \var SceneFickerOff
 * \brief No flickering detected.
 *
 * \var SceneFicker50Hz
 * \brief 50Hz flickering detected.
 *
 * \var SceneFicker60Hz
 * \brief 60Hz flickering detected.
 */

/**
 * \var SceneFlickerValues
 * \brief List of all SceneFlicker supported values
 */

/**
 * \var SceneFlicker
 * \brief Control to report the detected scene light frequency. Currently
 * identical to ANDROID_STATISTICS_SCENE_FLICKER.
 */

/**
 * \var PipelineDepth
 * \brief Specifies the number of pipeline stages the frame went through from when
 * it was exposed to when the final completed result was available to the
 * framework. Always less than or equal to PipelineMaxDepth. Currently
 * identical to ANDROID_REQUEST_PIPELINE_DEPTH.
 *
 * The typical value for this control is 3 as a frame is first exposed,
 * captured and then processed in a single pass through the ISP. Any
 * additional processing step performed after the ISP pass (in example face
 * detection, additional format conversions etc) count as an additional
 * pipeline stage.
 */

} /* namespace draft */

#ifndef __DOXYGEN__
/*
 * Keep the controls definitions hidden from doxygen as it incorrectly parses
 * them as functions.
 */
extern const Control<bool> AeEnable(AE_ENABLE, "AeEnable");
extern const Control<bool> AeLocked(AE_LOCKED, "AeLocked");
extern const std::array<const ControlValue, 4> AeMeteringModeValues = {
	static_cast<int32_t>(MeteringCentreWeighted),
	static_cast<int32_t>(MeteringSpot),
	static_cast<int32_t>(MeteringMatrix),
	static_cast<int32_t>(MeteringCustom),
};
extern const Control<int32_t> AeMeteringMode(AE_METERING_MODE, "AeMeteringMode");
extern const std::array<const ControlValue, 4> AeConstraintModeValues = {
	static_cast<int32_t>(ConstraintNormal),
	static_cast<int32_t>(ConstraintHighlight),
	static_cast<int32_t>(ConstraintShadows),
	static_cast<int32_t>(ConstraintCustom),
};
extern const Control<int32_t> AeConstraintMode(AE_CONSTRAINT_MODE, "AeConstraintMode");
extern const std::array<const ControlValue, 4> AeExposureModeValues = {
	static_cast<int32_t>(ExposureNormal),
	static_cast<int32_t>(ExposureShort),
	static_cast<int32_t>(ExposureLong),
	static_cast<int32_t>(ExposureCustom),
};
extern const Control<int32_t> AeExposureMode(AE_EXPOSURE_MODE, "AeExposureMode");
extern const Control<float> ExposureValue(EXPOSURE_VALUE, "ExposureValue");
extern const Control<int32_t> ExposureTime(EXPOSURE_TIME, "ExposureTime");
extern const Control<float> AnalogueGain(ANALOGUE_GAIN, "AnalogueGain");
extern const Control<float> Brightness(BRIGHTNESS, "Brightness");
extern const Control<float> Contrast(CONTRAST, "Contrast");
extern const Control<float> Lux(LUX, "Lux");
extern const Control<bool> AwbEnable(AWB_ENABLE, "AwbEnable");
extern const std::array<const ControlValue, 8> AwbModeValues = {
	static_cast<int32_t>(AwbAuto),
	static_cast<int32_t>(AwbIncandescent),
	static_cast<int32_t>(AwbTungsten),
	static_cast<int32_t>(AwbFluorescent),
	static_cast<int32_t>(AwbIndoor),
	static_cast<int32_t>(AwbDaylight),
	static_cast<int32_t>(AwbCloudy),
	static_cast<int32_t>(AwbCustom),
};
extern const Control<int32_t> AwbMode(AWB_MODE, "AwbMode");
extern const Control<bool> AwbLocked(AWB_LOCKED, "AwbLocked");
extern const Control<Span<const float>> ColourGains(COLOUR_GAINS, "ColourGains");
extern const Control<int32_t> ColourTemperature(COLOUR_TEMPERATURE, "ColourTemperature");
extern const Control<float> Saturation(SATURATION, "Saturation");
extern const Control<Span<const int32_t>> SensorBlackLevels(SENSOR_BLACK_LEVELS, "SensorBlackLevels");
extern const Control<float> Sharpness(SHARPNESS, "Sharpness");
extern const Control<int32_t> FocusFoM(FOCUS_FO_M, "FocusFoM");
extern const Control<Span<const float>> ColourCorrectionMatrix(COLOUR_CORRECTION_MATRIX, "ColourCorrectionMatrix");
extern const Control<Rectangle> ScalerCrop(SCALER_CROP, "ScalerCrop");
extern const Control<float> DigitalGain(DIGITAL_GAIN, "DigitalGain");
extern const Control<Span<const int64_t>> FrameDurations(FRAME_DURATIONS, "FrameDurations");

namespace draft {

extern const std::array<const ControlValue, 3> AePrecaptureTriggerValues = {

	static_cast<int32_t>(AePrecaptureTriggerIdle),

	static_cast<int32_t>(AePrecaptureTriggerStart),

	static_cast<int32_t>(AePrecaptureTriggerCancel),

};

extern const Control<int32_t> AePrecaptureTrigger(AE_PRECAPTURE_TRIGGER, "AePrecaptureTrigger");

extern const std::array<const ControlValue, 3> AfTriggerValues = {

	static_cast<int32_t>(AfTriggerIdle),

	static_cast<int32_t>(AfTriggerStart),

	static_cast<int32_t>(AfTriggerCancel),

};

extern const Control<int32_t> AfTrigger(AF_TRIGGER, "AfTrigger");

extern const std::array<const ControlValue, 5> NoiseReductionModeValues = {

	static_cast<int32_t>(NoiseReductionModeOff),

	static_cast<int32_t>(NoiseReductionModeFast),

	static_cast<int32_t>(NoiseReductionModeHighQuality),

	static_cast<int32_t>(NoiseReductionModeMinimal),

	static_cast<int32_t>(NoiseReductionModeZSL),

};

extern const Control<int32_t> NoiseReductionMode(NOISE_REDUCTION_MODE, "NoiseReductionMode");

extern const std::array<const ControlValue, 3> ColorCorrectionAberrationModeValues = {

	static_cast<int32_t>(ColorCorrectionAberrationOff),

	static_cast<int32_t>(ColorCorrectionAberrationFast),

	static_cast<int32_t>(ColorCorrectionAberrationHighQuality),

};

extern const Control<int32_t> ColorCorrectionAberrationMode(COLOR_CORRECTION_ABERRATION_MODE, "ColorCorrectionAberrationMode");

extern const std::array<const ControlValue, 6> AeStateValues = {

	static_cast<int32_t>(AeStateInactive),

	static_cast<int32_t>(AeStateSearching),

	static_cast<int32_t>(AeStateConverged),

	static_cast<int32_t>(AeStateLocked),

	static_cast<int32_t>(AeStateFlashRequired),

	static_cast<int32_t>(AeStatePrecapture),

};

extern const Control<int32_t> AeState(AE_STATE, "AeState");

extern const std::array<const ControlValue, 7> AfStateValues = {

	static_cast<int32_t>(AfStateInactive),

	static_cast<int32_t>(AfStatePassiveScan),

	static_cast<int32_t>(AfStatePassiveFocused),

	static_cast<int32_t>(AfStateActiveScan),

	static_cast<int32_t>(AfStateFocusedLock),

	static_cast<int32_t>(AfStateNotFocusedLock),

	static_cast<int32_t>(AfStatePassiveUnfocused),

};

extern const Control<int32_t> AfState(AF_STATE, "AfState");

extern const std::array<const ControlValue, 4> AwbStateValues = {

	static_cast<int32_t>(AwbStateInactive),

	static_cast<int32_t>(AwbStateSearching),

	static_cast<int32_t>(AwbConverged),

	static_cast<int32_t>(AwbLocked),

};

extern const Control<int32_t> AwbState(AWB_STATE, "AwbState");

extern const Control<int64_t> SensorTimestamp(SENSOR_TIMESTAMP, "SensorTimestamp");

extern const Control<int64_t> SensorRollingShutterSkew(SENSOR_ROLLING_SHUTTER_SKEW, "SensorRollingShutterSkew");

extern const std::array<const ControlValue, 2> LensShadingMapModeValues = {

	static_cast<int32_t>(LensShadingMapModeOff),

	static_cast<int32_t>(LensShadingMapModeOn),

};

extern const Control<int32_t> LensShadingMapMode(LENS_SHADING_MAP_MODE, "LensShadingMapMode");

extern const std::array<const ControlValue, 3> SceneFlickerValues = {

	static_cast<int32_t>(SceneFickerOff),

	static_cast<int32_t>(SceneFicker50Hz),

	static_cast<int32_t>(SceneFicker60Hz),

};

extern const Control<int32_t> SceneFlicker(SCENE_FLICKER, "SceneFlicker");

extern const Control<int32_t> PipelineDepth(PIPELINE_DEPTH, "PipelineDepth");

} /* namespace draft */
#endif

/**
 * \brief List of all supported libcamera controls
 *
 * Unless otherwise stated, all controls are bi-directional, i.e. they can be
 * set through Request::controls() and returned out through Request::metadata().
 */
extern const ControlIdMap controls {
	{ AE_ENABLE, &AeEnable },
	{ AE_LOCKED, &AeLocked },
	{ AE_METERING_MODE, &AeMeteringMode },
	{ AE_CONSTRAINT_MODE, &AeConstraintMode },
	{ AE_EXPOSURE_MODE, &AeExposureMode },
	{ EXPOSURE_VALUE, &ExposureValue },
	{ EXPOSURE_TIME, &ExposureTime },
	{ ANALOGUE_GAIN, &AnalogueGain },
	{ BRIGHTNESS, &Brightness },
	{ CONTRAST, &Contrast },
	{ LUX, &Lux },
	{ AWB_ENABLE, &AwbEnable },
	{ AWB_MODE, &AwbMode },
	{ AWB_LOCKED, &AwbLocked },
	{ COLOUR_GAINS, &ColourGains },
	{ COLOUR_TEMPERATURE, &ColourTemperature },
	{ SATURATION, &Saturation },
	{ SENSOR_BLACK_LEVELS, &SensorBlackLevels },
	{ SHARPNESS, &Sharpness },
	{ FOCUS_FO_M, &FocusFoM },
	{ COLOUR_CORRECTION_MATRIX, &ColourCorrectionMatrix },
	{ SCALER_CROP, &ScalerCrop },
	{ DIGITAL_GAIN, &DigitalGain },
	{ FRAME_DURATIONS, &FrameDurations },
	{ AE_PRECAPTURE_TRIGGER, &draft::AePrecaptureTrigger },
	{ AF_TRIGGER, &draft::AfTrigger },
	{ NOISE_REDUCTION_MODE, &draft::NoiseReductionMode },
	{ COLOR_CORRECTION_ABERRATION_MODE, &draft::ColorCorrectionAberrationMode },
	{ AE_STATE, &draft::AeState },
	{ AF_STATE, &draft::AfState },
	{ AWB_STATE, &draft::AwbState },
	{ SENSOR_TIMESTAMP, &draft::SensorTimestamp },
	{ SENSOR_ROLLING_SHUTTER_SKEW, &draft::SensorRollingShutterSkew },
	{ LENS_SHADING_MAP_MODE, &draft::LensShadingMapMode },
	{ SCENE_FLICKER, &draft::SceneFlicker },
	{ PIPELINE_DEPTH, &draft::PipelineDepth },
};

} /* namespace controls */

} /* namespace libcamera */
