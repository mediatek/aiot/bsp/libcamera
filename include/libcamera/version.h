/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019, Google Inc.
 *
 * version.h - Library version information
 *
 * This file is auto-generated. Do not edit.
 */
#ifndef __LIBCAMERA_VERSION_H__
#define __LIBCAMERA_VERSION_H__

#define LIBCAMERA_VERSION_MAJOR		0
#define LIBCAMERA_VERSION_MINOR		0
#define LIBCAMERA_VERSION_PATCH		0

#endif /* __LIBCAMERA_VERSION_H__ */
