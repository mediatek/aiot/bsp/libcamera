/* SPDX-License-Identifier: LGPL-2.1-or-later */
/* This file is auto-generated, do not edit! */
/*
 * Copyright (C) 2018-2019, Google Inc.
 *
 * libcamera.h - libcamera public API
 */
#ifndef __LIBCAMERA_LIBCAMERA_H__
#define __LIBCAMERA_LIBCAMERA_H__

#include <libcamera/bound_method.h>
#include <libcamera/buffer.h>
#include <libcamera/camera.h>
#include <libcamera/camera_manager.h>
#include <libcamera/class.h>
#include <libcamera/compiler.h>
#include <libcamera/control_ids.h>
#include <libcamera/controls.h>
#include <libcamera/file_descriptor.h>
#include <libcamera/formats.h>
#include <libcamera/framebuffer_allocator.h>
#include <libcamera/geometry.h>
#include <libcamera/logging.h>
#include <libcamera/object.h>
#include <libcamera/pixel_format.h>
#include <libcamera/property_ids.h>
#include <libcamera/request.h>
#include <libcamera/signal.h>
#include <libcamera/span.h>
#include <libcamera/stream.h>
#include <libcamera/transform.h>
#include <libcamera/version.h>

#endif /* __LIBCAMERA_LIBCAMERA_H__ */
